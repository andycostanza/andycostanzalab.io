+++
title = "Renommer la branche master de votre dépôt Git en main "
tags = ["git","ci","devops","gitea"]
categories = ["Dev"]
author = "Andy Costanza"
comments = true
layout = "post"
date = "2022-08-16T11:15:50+01:00"
+++
Après la mise à jour du plugin [net.researchgate:gradle-release](https://github.com/researchgate/gradle-release) en v3.0.0, le fonctionnement de ma CI a été altéré, car pour lui, [la branche principale doit être main](https://github.com/researchgate/gradle-release/blob/cc383f6e8cf4fbd04ebb0864499cd072b9f9a6ad/src/main/groovy/net/researchgate/release/GitAdapter.groovy#L56) et pas master

## Renommer votre branche locale
```bash
> git branch -m master main
```

Vérifier ensuite que tout est ok niveau commit
```bash
> git status
Sur la branche main
Votre branche est en avance sur 'origin/master' de 1 commit.
  (utilisez "git push" pour publier vos commits locaux)
 
rien à valider, la copie de travail est propre
```

## Créer la branche sur le remote
```bash
> git push -u origin main
Énumération des objets: 7, fait.
Décompte des objets: 100% (7/7), fait.
Compression par delta en utilisant jusqu'à 12 fils d'exécution
Compression des objets: 100% (4/4), fait.
Écriture des objets: 100% (4/4), 1.06 Kio | 1.06 Mio/s, fait.
Total 4 (delta 3), réutilisés 0 (delta 0)
remote:
remote: Create a new pull request for 'main':
remote:   https://git.myhost.com/.../compare/master...main
remote:
remote: . Processing 1 references
remote: Processed 1 references in total
To git.myhost.com:...git
 * [new branch]      main -> main
La branche 'main' est paramétrée pour suivre la branche distante 'main' depuis 'origin'.
```

Sur votre serveur Git, activer main comme branche par défaut.

Dans Gitea, il faut cliquer sur Settings et dans le tab Branches, sélectionner main et cliquer sur le bouton Update Default Branch:

![Paramètres de configuration gitea](images/gitea.png)

## Supprimer la branche master sur le remote
```bash
> git push origin --delete master
remote: . Processing 1 references
remote: Processed 1 references in total
To git.myhost.com:...git
 - [deleted]         master
 ```
