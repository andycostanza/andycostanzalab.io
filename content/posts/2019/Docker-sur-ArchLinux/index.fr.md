---
author: Andy Costanza
comments: true
title: "Docker sur ArchLinux"
slug: Docker-sur-ArchLinux
title: Docker sur ArchLinux
date: 2019-09-19T16:15:50+01:00
layout: post
tags:
 - spring
 - docker
 - java
 - spring boot
categories: 
 - GNU/Linux
---

Voici un petit guide de mise en route pour déployer une application Spring Boot dans un conteneur docker sur ArchLinux

## Pré-requis
Les commandes ci-dessous sous exécutée sur ArchLinux.
### Installation de docker et de ses autres outils
```bash
$ sudo pacman -Syu docker docker-compose docker-machine
$ sudo systemctl start docker
$ sudo systemctl enable docker
```
### Modification des droits d'accès de l'utilisateur

> __ATTENTION__ : <br/>
> If you want to be able to run docker as a regular user, [add your user to the docker user group](https://wiki.ArchLinux.org/index.php/Docker#Installation). 
> Anyone added to the docker group is root equivalent. More information [here](https://github.com/docker/docker/issues/9976) and [here](https://docs.docker.com/engine/security/security/).

```bash
$ sudo gpasswd -a ${USER} docker
$ sudo reboot
```
Sous GNOME j'ai dû redémarrer la machine car un simple déconnexion de session n'a pas suffi.

## Testons la configuration
### Est-ce que Docker est bien configuré
```bash
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```
Si on vous demande un mot de passe c'est que vous avez merdé lors de l'ajout du groupe docker à votre utilisateur

### Récupérons un simple projet Spring Boot, compilons le
```bash
$ mkdir Projets
$ cd Projets
$ git clone https://gitlab.com/docker-spring-cloud/docker-demo.git
$ cd docker-demo
$ ./mvnw install dockerfile:build
```
Félicitation, vous venez de compiler et de "_conteneuriser_" votre première application Spring Boot

### Démarrons le conteneur Docker
```bash
$ docker image ls
REPOSITORY                     TAG                 IMAGE ID            CREATED             SIZE
andycostanzacom/docker-demo    latest              18c5d16986d9        2 hours ago         119MB
openjdk                        8-jdk-alpine        97bc1352afde        7 weeks ago         103MB
$ docker run -p 8080:8080 -t andycostanzacom/docker-demo
```
Dans votre navigateur internet, ouvrez la page http://localhost:8080/hello et si la page vous répond "hello" c'est que tout va bien, vous venez d'appeler un endpoint Spring Boot dans un conteneur docker.

## Trucs et astuces
Pour savoir quel conteneur est en cours de fonctionnement :
```bash
$ docker ps
CONTAINER ID        IMAGE                         COMMAND                  CREATED             STATUS              PORTS                    NAMES
43721a2aff5d        andycostanzacom/docker-demo   "java -cp app:app/li…"   8 seconds ago       Up 7 seconds        0.0.0.0:8080->8080/tcp   hungry_greider
```

Pour arrêter un conteneur :
```bash
$ docker stop 43721a2aff5d
43721a2aff5d
```
Ouais je sais, il pourrait être plus explicite que de renvoyer juste le _Container ID_!